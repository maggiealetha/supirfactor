<pre style="line-height: 1.2;">


</pre>
Deep learning method for inferring GRNs
================================================================================================

## Summary

<p align="center"><img src="" width="80%" /></p>

<!-- Install latest version through pip -->
<!-- ``` -->
<!-- pip install dewakss -->
<!-- ``` -->
<!-- For best results DEWAKSS require the MKL from intel which should be default in any conda install.  -->
<!-- If DEWAKSS complains please check how to get MKL from [intel](https://software.intel.com/content/www/us/en/develop/tools/math-kernel-library.html) or [anaconda](https://docs.anaconda.com/mkl-optimizations/). -->

Install latest version by cloning this repository
```
git clone https://gitlab.com/Xparx/superfactor.git
cd superfactor
```
and then in the superfactor directory:
```
pip install .
```

## Usage

<!-- The simplest way to use DEWAKSS is to simply run the following -->

<!--     import dewakss.denoise as dewakss -->
    
<!--     dewaxer = dewakss.DEWAKSS(adata) -->
<!--     dewaxer.fit(adata) -->
<!--     dewaxer.transform(adata, copy=False) -->

<!-- where `adata` is either an expression matrix or an [AnnData](https://scanpy.readthedocs.io/en/stable/) object with genes as columns and cells/samples as rows. -->

<!-- To explore the results one can use -->

<!--     dewaxer.plot_global_performance() -->

<!-- If one chooses to run diffusion: -->

<!--     N=6 -->
<!--     dewaxer = dewakss.DEWAKSS(adata, iterations=N) -->

<!-- these can be explored using -->

<!--     dewaxer.plot_diffusion_performance() -->
