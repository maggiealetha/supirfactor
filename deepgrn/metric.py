import torch as _torch
from torch import nn as _nn
from deepgrn.utils import get_device


def BIC(data_loader, model, criteria='bic', lossf=_nn.MSELoss(reduction='none'), device=get_device(), eps=_torch.finfo(_torch.float32).eps):
    """Compute the Bayesian information criterion"""

    feat_loss = 0
    feat_var = 0

    model = model.eval()
    with _torch.no_grad():

        for name, coef in model.named_parameters():
            if 'grn' in name:
                df = _torch.sum(coef.abs() > eps, 1)

        # coef = model.decoder.linear.weight.data

        for batch_features in data_loader:

            if isinstance(batch_features, list):
                batch_features = batch_features[0]

            n_samples = batch_features.shape[0]
            if criteria == 'aic':
                K = 2  # AIC
            elif criteria == 'bic':
                K = _torch.log(_torch.tensor(n_samples).float())  # BIC
            else:
                raise ValueError('criterion should be either bic or aic')

            # print((_torch.abs(coef) > _torch.finfo(coef.dtype).eps).shape)
            # df = _torch.sum(_torch.abs(coef) > _torch.finfo(coef.dtype).eps, 1)

            batch_features = batch_features.to(device)

            ypred = model(batch_features)

            # print('ypred ', ypred.shape)
            # print('ypred ', ypred)

            # print('batch ', batch_features.shape)
            # print('batch ', batch_features)
            # print(lossf)

            test_loss = lossf(ypred, batch_features)

            # print('test loss', test_loss.shape)
            # print('test loss', test_loss)

            feat_loss += test_loss.sum(0)
            # print('feat loss', feat_loss.shape)
            # print('feat loss', feat_loss)

            sigma2 = _torch.var(batch_features, 0)

            feat_var += sigma2

        feat_var = feat_var / len(data_loader)
        test_loss = test_loss / len(data_loader)
        eps = _torch.finfo(feat_var.dtype).eps
        # print('sigma2', sigma2.shape)
        # print('feat_loss', feat_loss.shape)
        # print('n_samples', n_samples)
        # print('K', K)
        # print('df', df.shape)
        bi_ = (n_samples * feat_loss / (sigma2 + eps) + K * df)  # Eqns. 2.15--16 in (Zou et al, 2007)

        # print(bi_.shape)

    # n_best = _np.argmin(self.criterion_)
    return bi_
