import torch as _torch
from torch import nn as _nn
# from torch.autograd import Variable


def get_device():

    if _torch.cuda.is_available():
        device = 'cuda:0'
    else:
        device = 'cpu'
    return _torch.device(device)


class BaseModel(_nn.Module):
    """Documentation for GenericModel

    """
    def __init__(self, mask=None):
        super(BaseModel, self).__init__()
        # self.args = args
        self.mask = mask

    def mask_prior(self, grad):

        masked_grad = grad.mul(self.mask) if self.mask is not None else grad
        return masked_grad

    def mask_zeros(self, grad):

        tmpmask = (self.linear.weight.abs() > 1e-10).float()
        masked_grad = grad.mul(tmpmask)
        # print(masked_grad.bool().sum().item())
        return masked_grad
