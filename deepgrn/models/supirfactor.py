# import torchvision as _torchvision
from torch import nn as _nn
import torch as _torch
from deepgrn.models import BaseModel as _bm, get_device
# import numpy as _np
# from torch.autograd import Variable
# from torch.utils.data import DataLoader
# from torchvision import transforms
from copy import deepcopy
# from torch.nn import functional as _F


class Encoder(_bm):

    def __init__(self, mask, p=0.0, bias=False, dobnorm=False, device=get_device(), activation=None):
        super().__init__()

        self.mask = mask.to(device)
        hidden_width, input_shape = mask.shape
        self.dobnorm = dobnorm
        self.dop = _nn.Dropout(p=p)
        if dobnorm:
            self.bnorm = _nn.BatchNorm1d(input_shape)
        self.linear = _nn.Linear(in_features=input_shape, out_features=hidden_width, bias=bias)

        self.fix_mask_init(device)
        self.activation = activation

        # if fine_tune:
        #     self.linear.weight.register_hook(self.mask_zeros)

    def fix_mask_init(self, device):
        weight = self.linear.weight.to(device)
        weight = weight.mul(self.mask.abs())
        weight[weight == -0.0] = 0.0
        self.linear.weight = _nn.parameter.Parameter(weight)
        self.linear.weight.register_hook(self.mask_prior)

    def forward(self, x, lof=None):

        if self.dobnorm:
            x = self.bnorm(x)
        x = self.dop(x)
        x = self.linear(x)
        if self.activation is not None:
            x = self.activation(x)

        return x


class Decoder(_bm):

    def __init__(self, output_shape=100, hidden_width=10, bias=False, p=0.0, dobnorm=False, activation=None, mask=None, device=get_device()):
        super().__init__()

        self.linear = _nn.Linear(in_features=hidden_width, out_features=output_shape, bias=bias)
        self.dop = _nn.Dropout(p=p)
        self.dobnorm = dobnorm
        self.activation = activation

        if dobnorm:
            self.bnorm = _nn.BatchNorm1d(hidden_width)

        if mask is not None:
            self.mask = mask.to(device)
            self.fix_mask_init(device)
            self.linear.weight.register_hook(self.mask_prior)

    def fix_mask_init(self, device):
        weight = self.linear.weight.to(device)
        weight = weight.mul(self.mask.abs())
        weight[weight == -0.0] = 0.0
        self.linear.weight = _nn.parameter.Parameter(weight)
        self.linear.weight.register_hook(self.mask_prior)

    def forward(self, x):

        if self.dobnorm:
            x = self.bnorm(x)
        x = self.dop(x)
        x = self.linear(x)
        if self.activation is not None:
            x = self.activation(x)
        return x


class SupirFactor(_nn.Module):

    def __init__(self, mask, mask_grn=None, pin=0.0, ptf=0.0, dobnorm_input=False, dobnorm_output=False, device=get_device(), prior_activation=None, grn_activation=None, bias=False):

        super().__init__()
        hidden_width, input_shape = mask.shape

        self.prior = Encoder(mask, bias=bias, p=pin, dobnorm=dobnorm_input, device=device, activation=prior_activation)
        self.grn = Decoder(output_shape=input_shape, hidden_width=hidden_width, bias=bias, p=ptf, dobnorm=dobnorm_output, activation=grn_activation, device=device, mask=mask_grn)

        self.init_state = deepcopy(self.state_dict())

        for k in self.init_state:
            self.init_state[k] = self.init_state[k].to(device)

    def reset_init(self):

        self.load_state_dict(deepcopy(self.init_state))

    # def reset_init_nonzero(self):

    #     state = self.init_state
    #     for name, param in self.named_parameters():

    #         state[name] = self.init_state[name].to(state[name].device).mul(param.bool().float())

    #     self.load_state_dict(deepcopy(state))

    def forward(self, x):
        x = self.prior(x)
        x = self.grn(x)
        return x


class TFTFmodule(_bm):
    """Documentation for TFTFmodule

    """
    def __init__(self, output_shape=None, input_width=None, bias=False, device=get_device(), p=0.0, activation=None):
        super().__init__()

        self.dop = _nn.Dropout(p=p)
        self.linear = _nn.Linear(in_features=input_width, out_features=output_shape, bias=bias)
        self.activation = activation

        weight = self.linear.weight.to(device)
        weight = _torch.eye(*weight.shape).to(device)
        self.linear.weight = _nn.parameter.Parameter(weight)
        self.linear.weight.register_hook(self.keep_diagonal_steady)

        # if fine_tune:
        #     self.linear.weight.register_hook(self.mask_zeros)

    def keep_diagonal_steady(self, grad):

        masked_grad = grad.mul(1 - _torch.eye(*grad.shape).to(grad.device))
        return masked_grad

    def forward(self, x):

        x = self.dop(x)
        x = self.linear(x)
        if self.activation is not None:
            x = self.activation(x)

        return x


class SFTFTF(SupirFactor):

    def __init__(self, mask, mask_grn=None, bias=False, pin=0.0, ptf=0.0, dobnorm_input=False, dobnorm_output=False, device=get_device(), prior_activation=None, tfa_activation=None, grn_activation=None):

        super().__init__(mask, mask_grn=mask_grn, bias=bias, pin=pin, ptf=ptf, dobnorm_input=dobnorm_input, dobnorm_output=dobnorm_output, device=device, prior_activation=prior_activation, grn_activation=grn_activation)

        hidden_width, input_shape = mask.shape

        self.tfa = TFTFmodule(input_width=hidden_width, output_shape=hidden_width, p=ptf, bias=bias, device=device, activation=tfa_activation)

        self.init_state = deepcopy(self.state_dict())

    def reset_init(self):

        self.load_state_dict(deepcopy(self.init_state))

    def forward(self, x):
        x = self.prior(x)
        x = self.tfa(x)
        x = self.grn(x)
        return x
