import torch as _torch
# import torchvision as _torchvision
from torch import nn as _nn
import numpy as _np
import pandas as _pd
from deepgrn.models import supirfactor as sfgrn
# from deepgrn.models import AEabs as aegrn
# from deepgrn import AE as aegrn
# from torch.autograd import Variable
# from torch.utils.data import DataLoader
# from torchvision import transforms
from deepgrn.utils import get_device


def loop(model, train_loader, optimizer, criterion, epochs, test_loader=None, device=get_device(), verbose=False, test_freq=10):

    objctive = []
    test_obj = []
    for epoch in range(epochs):

        tr_loss = train_reconstruction(model, train_loader, device, optimizer, criterion)

        objctive.append(tr_loss)

        if (not (epoch % test_freq)) and (test_loader is not None):
            testr_loss = test_reconstruction(model, test_loader, criterion, device=device)
            test_obj.append(testr_loss)

        if not (epoch % 10) and verbose:
            # display the epoch training loss

            print(f"epoch : {epoch}/{epochs}, loss = {tr_loss:.6f}, test = {testr_loss:.6f}")

    if test_loader is not None:
        testr_loss = test_reconstruction(model, test_loader, criterion, device=device)
    else:
        testr_loss = _np.nan

    test_obj.append(testr_loss)

    return objctive, test_obj


def train_reconstruction(model, train_loader, device, optimizer, criterion):

    model.train()
    loss = 0
    for batch_features in train_loader:
        batch_features = batch_features.to(device)

        optimizer.zero_grad()

        # compute reconstructions
        outputs = model(batch_features)

        # compute training reconstruction loss
        train_loss = criterion(outputs, batch_features)

        # compute accumulated gradients
        train_loss.backward()

        # perform parameter update based on current gradients
        optimizer.step()

        # add the mini-batch training loss to epoch loss
        loss += train_loss.item()

    # compute the epoch training loss
    loss = loss / len(train_loader)

    return loss


def test_reconstruction(model, test_loader, criterion, device=get_device()):

    loss = 0
    var = 0
    model = model.eval()
    with _torch.no_grad():
        for batch_features in test_loader:

            batch_features = batch_features.to(device)

            outputs = model(batch_features)

            test_loss = criterion(outputs, batch_features)
            test_var = criterion(_torch.zeros(batch_features.shape).to(device), batch_features)

            loss += test_loss.item()
            var += test_var.item()

        # Currently R2
        loss = 1 - (loss / var)  # len(test_loader)

    return loss


def truncate_weights(component, zeta=0.0, verbose=True, per_row=True, transpose=False, device=get_device()):

    if not per_row:
        dw = component.weight.data.abs()
        data = component.weight.data.clone()
        dw = dw[dw != 0]
        elements = int(zeta * dw.numel())
        mask_weights = (data.abs() >= dw.kthvalue(elements).values).float()
        component.weight = _nn.parameter.Parameter(data.mul(mask_weights))

    else:
        dw = component.weight.data.abs()
        data = component.weight.data.clone()
        if transpose:
            dw = dw.T
            data = data.T
        else:
            dw = component.weight.data.abs()
            data = component.weight.data.clone()

        mask_weights = _torch.zeros(data.shape).float().to(device)
        for row in _torch.arange(data.shape[0]):
            dwt = dw[row, :]
            dwt = dwt[dwt != 0]
            elements = int(zeta * dwt.numel())
            mask_weights[row, :] = (data[row, :].abs() >= dwt.kthvalue(max(elements, 1)).values).float()

        if transpose:
            component.weight = _nn.parameter.Parameter(data.T.mul(mask_weights.T))
        else:
            component.weight = _nn.parameter.Parameter(data.mul(mask_weights))

    if verbose:
        print(f'number of nonzero before truncation: {dw.numel()}')
        print(f'setting {elements} elements = 0')
        print(f'nonzero elements: {mask_weights.sum()}')


def setup_data(X, prior, Xtest=None, test_size=0.25, astype=_np.float32, device=get_device(), random_state=42, fraction_batch=10, shuffle_samples=True):

    # from sklearn.model_selection import train_test_split

    # Xtn, Xtt = train_test_split(X, test_size=test_size, random_state=random_state, shuffle=shuffle_samples)
    # randrange = _np.random.permutation(range(X.shape[0]))

    # X = _torch.from_numpy(X).to(device) # Does not work.
    train_loader = _torch.utils.data.DataLoader(X, batch_size=int(X.shape[0] // fraction_batch), shuffle=True, num_workers=4, pin_memory=True)

    if Xtest is not None:
        test_loader = _torch.utils.data.DataLoader(Xtest, batch_size=int(Xtest.shape[0] // fraction_batch), shuffle=False, num_workers=4)
    else:
        test_loader = None

    # mask = torch.from_numpy(filtered_prior.values.astype(np.float32)).to(device)
    prior = prior.copy() if isinstance(prior, _np.ndarray) else prior.values.copy()
    mask = _torch.from_numpy(prior.astype(astype)).to(device)

    return train_loader, test_loader, mask


def shuffle2d(data, astype=_np.float32):

    sh = data.shape
    # # data = np.random.randn(*sh).astype(np.float32)
    data = data.ravel().copy()
    _np.random.shuffle(data)
    data = data.reshape(sh).astype(astype)
    return data


def modeleval_loop(X, prior, Xtest=None, criterion=_nn.MSELoss(), device=get_device(), weight_decay=1e-3, epochs=100, dobnorm=False, fraction_batch=10, pin=0.0, ptf=0.0, verbose=False, lambd=0.0, lr=1e-3, model=sfgrn.SupirFactor, test_freq=10):

    # testl = []
    train_loader, test_loader, mask = setup_data(X, prior, Xtest=Xtest, fraction_batch=fraction_batch)

    # model = aegrn.AutoEncoder(input_shape=mask.shape[1], hidden_width=mask.shape[0], mask=mask, pin=pin, ptf=ptf, bias=False, dobnorm=dobnorm, lambd=lambd).to(device)
    model = model(input_shape=mask.shape[1], hidden_width=mask.shape[0], mask=mask, pin=pin, ptf=ptf, bias=False, dobnorm=dobnorm, lambd=lambd).to(device)

    optimizer = _torch.optim.Adam(model.parameters(), lr=lr, weight_decay=weight_decay)

    loss, test_loss = loop(model, train_loader, optimizer, criterion, epochs, test_loader=test_loader, device=device, verbose=verbose, test_freq=test_freq)
    # print(test_loss)
    test_loss = _pd.DataFrame(test_loss, columns=['Test'])
    test_loss['lambda'] = lambd
    # testl.append(test_loss)

    losses = _pd.DataFrame(loss, columns=['Train'])
    losses['lambda'] = lambd

    return model, losses, test_loss


def extract_weights(model, prior):

    with _torch.no_grad():
        W1 = model.prior.linear.weight.data.cpu().clone().detach()
        W1[_torch.abs(W1) < _torch.finfo(W1.dtype).eps] = 0

        W2 = model.grn.linear.weight.data.cpu().clone().detach()
        W2[_torch.abs(W2) < _torch.finfo(W2.dtype).eps] = 0

        output_weights = _pd.DataFrame(W2.T.numpy(), index=prior.index if prior is not None else prior, columns=prior.columns if prior is not None else prior)
        output_weights = output_weights.replace(-0.0, 0.0)

        input_weights = _pd.DataFrame(W1.numpy(), index=prior.index if prior is not None else prior, columns=prior.columns if prior is not None else prior)
        input_weights = input_weights.replace(-0.0, 0.0)

    return output_weights, input_weights


def compare2gs(weights, gs):

    gs = gs.loc[:, (gs.abs().sum(0) > 0).values]
    weights = weights.reindex_like(gs).fillna(0)
    TPw = weights.multiply(gs.abs())

    return weights, TPw


def split_prior_v1(prior, rng=_np.random.default_rng(42), fraction_gs=0.1):

    if (prior.sum(1) == 0).sum() != 0:
        print('Prior have 0s in TF, can not split.\nReturning without change')
        return prior, None

    use_prior = prior.copy()

    use_prior = use_prior.loc[:, ~(use_prior.astype(bool).sum(0) == 0)].copy()
    rows, cols = use_prior.shape

    numzero = 1
    counter = 0
    while numzero > 0:

        colsids = _np.arange(cols)

        rng.shuffle(colsids)

        colsids = sorted(colsids[:int(cols * fraction_gs)])
        # print(colsids)
        gs = use_prior.iloc[:, colsids]

        tmp = use_prior.copy()
        tmp.iloc[:, colsids] = 0
        numzero = (tmp.astype(bool).sum(1) == 0).sum()

        counter += 1

        if not (counter % 100):
            print(f'Attemts to create full prior: {counter}')

    use_prior.iloc[:, colsids] = 0

    use_prior = use_prior.reindex(columns=prior.columns, fill_value=0)

    gs = gs[gs.sum(1) > 0]

    return use_prior, gs


def split_prior(prior, gold_standard=None, rng=_np.random.default_rng(42), fraction_gs=0.1):

    if (prior.sum(1) == 0).sum() != 0:
        print('Prior have 0s in TF, can not split.\nReturning without change')
        return prior, None

    use_prior = prior.copy()

    use_prior = use_prior.loc[:, ~(use_prior.astype(bool).sum(0) == 0)].copy()

    if gold_standard is not None:
        use_gs = gold_standard.astype(bool).astype(int).reindex(columns=use_prior.columns, fill_value=0)
    else:
        use_gs = None

    rows, cols = use_prior.shape

    numzero = 1
    counter = 0
    while numzero > 0:

        colsids = _np.arange(cols)

        rng.shuffle(colsids)

        colsgs = sorted(colsids[:int(cols * fraction_gs)])
        colspri = sorted(colsids[int(cols * fraction_gs):])
        tmp = use_prior.copy()
        tmp.iloc[:, colsgs] = 0
        numzero = (tmp.astype(bool).sum(1) == 0).sum()

        counter += 1

        if not (counter % 100):
            print(f'Attemts to create full prior: {counter}')

    use_prior.iloc[:, colsgs] = 0
    use_prior = use_prior.reindex(columns=prior.columns, fill_value=0)
    # print(colsids)

    if use_gs is not None:
        use_gs.iloc[:, colspri] = 0
        gs = use_gs.loc[:, use_gs.abs().sum(0) > 0].copy()
        # gs = use_gs.copy()
    else:
        gs = use_prior.iloc[:, colsgs]

    # gs = gs[gs.sum(1) > 0]

    return use_prior, gs


def compute_performance_opt_mcc(infmetric, mcc_vec):
    """Returns optimal metrics given max MCC"""

    # pr.confidence_data.loc[mcc.confidence_data['MCC'].idxmax()]

    optmcc = mcc_vec.idxmax()

    optconf = infmetric.loc[optmcc]['combined_confidences']

    sizegs = infmetric['gold_standard'].sum()

    gsopt = infmetric[infmetric['combined_confidences'] > optconf]['gold_standard'].sum()

    fracgs = gsopt / sizegs

    opt_precision = infmetric.loc[optmcc]['precision']

    opt_recall = infmetric.loc[optmcc]['recall']

    return (optconf, fracgs, opt_recall, opt_precision)
