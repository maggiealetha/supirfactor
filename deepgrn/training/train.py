import os as _os
import torch as _torch
# import torchvision as _torchvision
from torch import nn as _nn
# from torch.nn import functional as _F
import numpy as _np
import scipy as _sp
import pandas as _pd
from deepgrn.models import supirfactor as sfgrn
from deepgrn.training import split_prior, compute_performance_opt_mcc, extract_weights, shuffle2d as _randomize
from deepgrn.utils import get_device, _get_layer_data
from deepgrn.metric import BIC as _bic
from sklearn.preprocessing import scale
from sklearn.preprocessing import StandardScaler
# from sklearn.model_selection import ParameterGrid as _grid
from copy import deepcopy as _deepcopy
from inferelator.postprocessing.model_metrics import RankSummaryPR as _RSPR
from inferelator.postprocessing.model_metrics import RankSummaryMCC as _RSMCC
from tabulate import tabulate as _tbl

import inferelator.utils as utils
utils.Debug.set_verbose_level(-1)


_LOG_PATH = _os.path.join('/', 'tmp', 'supirfactor')
_NETGROUPS = ['layer', 'random P', 'random X', 'optimizer', 'regulation', 'ftzeta', 'zeta']


class trainGRN(object):
    """trainGRN is a helper class for training prior knowledge primed gene regulatory networks (GRNs).

    """
    def __init__(self,
                 prior,
                 gold_standard=None,
                 model_class=sfgrn.SupirFactor,
                 optimizer_class=_torch.optim.Adam,
                 loss_function=_nn.MSELoss,
                 regularizer=None,
                 device=get_device(),
                 model_kwargs={},
                 data_kwargs={'test_size': 0.1, 'validation_size': 0.2},
                 loss_kwargs={},
                 err_targets_in_prior=True,
                 model_params_regularized=None,
                 cv_prior=True,
                 fraction_gs=0.2,
                 bootstrap=False,
                 n_resamples=0,
                 start_swa=0.5,
                 with_mean=True,
                 seed=None,
                 dtype=_torch.float32,
                 logtb=False,
                 result_file=_LOG_PATH,
                 verbose=False,
                 eps=None,
                 zero_break=True):

        """"
        Parameters
        ----------
        prior : pandas.Dataframe,
            A dataframe containing prior evidence of interactions (TF x gene (rows x columns)).
        gold_standard : pandas.Dataframe,
            A dataframe with the same format as prior containing gold standard connections. If not provided will use the prior network (TF x gene (rows x columns)).
        model_class : torch.nn class,
            The class of the model to be trainined (default SupirFactor).
        optimizer_class : torch.optim,
            The optimizer to be used to minimize the objective (default torch.optim.Adam).
        loss_function : torch.nn class,
            The loss function class to be used when optimizing (default torch.nn.MSELoss).
        regularizer : regularizer class,
            A regularization function from https://github.com/Xparx/pyTorchReg, needs to similar at least (default None).
        device : torch.device,
            The device to be used for computation, (default inferred).
        model_kwargs : dict,
            Default model key value pairs passed to each model genereated, (default {}).
        data_kwargs : dict,
            Default model key value pairs passed to the data generation, (default {'test_size': 0.1, 'validation_size': 0.2}).
        loss_kwargs : dict,
            Default loss key value pairs passed to each loss function genereated, (default {}).
        err_targets_in_prior : bool,
            If test error should be computed on all targets or just targets not in the prior, if True compute on all targets, if False only compute on non prior target, (default True).
        model_params_regularized : list,
            List or parameters in the model that shoul be added to the regularization term as found by model.named_parameters(), (default None = use all).
        cv_prior : bool,
            If the prior for every resample should be cross validated. If both prior and gold standard is provided a subset of targets are removed from the prior with the corresponding set remaining in the gold standard and tested against. If the gold standard is not provided the prior is used as a gold standard with the same procedure, (default True).
        fraction_gs : float,
            What fraction of the gold standard should be tested against, (default 0.2).
        bootstrap : bool
            If the data should be bootstrapped for each resample. If True the data will be resampled with replacement and used, (default False).
        n_resamples : int,
            How many times should the data be resampled, 0 means the training is run ones, default 0.
        start_swa : float,
            At what fraction of epochs should we start to collected data for stochastic weight averaging, (default 0.5) NOT IMPLEMENTED YET.
        with_mean : bool,
            Should standard normalization normalize by removing the mean, (default True).
        seed : int,
            what seed to use for the random number generator, (default None).
        dtype : torch.dtype
            what data type should be used by default, (default torch.float32).
        logtb : bool,
            Should we use tensorboard or not, (default False) NOT IMPLEMENTED YET.
        result_file : path,
            path to use for results such as tensorboard output, (default os.path.join('/', 'tmp', 'supirfactor')).
        verbose : bool,
            should results be printed during runtime, (default False).
        eps : float,
            What precision on values should be used, (default torch.finfo(dtype).eps)
        zero_break: bool,
            Should we stop iterating when reaching a zero network, (default True).
        """

        if logtb:
            try:
                import tensorboard
            except ModuleNotFoundError as err:
                # Error handling
                Warning(err)
                logtb = False

        super(trainGRN, self).__init__()

        self.astype = dtype
        self.device = device
        self.model_class = model_class
        self.default_model_kwargs = model_kwargs
        self.prior = prior
        self.gold_standard = gold_standard
        self.optimizer_class = optimizer_class
        self.loss_function = loss_function
        self.default_loss_kwargs = loss_kwargs
        self.default_data_kwargs = data_kwargs
        if eps is None:
            self.eps = _torch.finfo(dtype).eps
        else:
            self.eps = eps
        self.global_epoch = 0
        self.train_loss = []
        self.err_targets_in_prior = err_targets_in_prior
        self.rng = _np.random.default_rng(seed)
        self.verbose = verbose
        self.with_mean = with_mean
        self.n_resamples = n_resamples
        self.validation_loss = []
        self.result_file = result_file
        self.fraction_gs = fraction_gs
        self.regularizer = regularizer
        self.model_params_regularized = model_params_regularized
        self.bootstrap = bootstrap
        self.cv_prior = cv_prior
        self._models = {}
        self._start_swa = start_swa
        self._zero_break = zero_break

    def split_samples(self, X, y=None, test_size=0.1, validation_size=0.1, **kwargs):

        scaler = StandardScaler(with_mean=self.with_mean)
        samples = _np.arange(X.shape[0])
        self.rng.shuffle(samples)
        X = X.A if _sp.sparse.issparse(X) else X

        if self._randomize_data:
            X = _randomize(X)

        train, validate, test = _np.split(samples, [int((1 - validation_size - test_size) * len(samples)), int((1 - test_size) * len(samples))])

        Xtrain = scaler.fit_transform(X[train, :].copy())
        Xval = scaler.fit_transform(X[validate, :].copy())
        Xtest = scaler.fit_transform(X[test, :].copy())

        return Xtrain, Xval, Xtest

    def setup_data(self, X, Xval=None, Xtest=None, batch_size=1024, **kwargs):

        if self.bootstrap:
            bs = self.rng.integers(0, X.shape[0], X.shape[0])
            datas = _torch.utils.data.TensorDataset(_torch.tensor(X[bs, :]))
        else:
            # paired = torch.utils.data.TensorDataset(_torch.tensor(X[train, :]), torch.tensor(y))
            datas = _torch.utils.data.TensorDataset(_torch.tensor(X))

        train_loader = _torch.utils.data.DataLoader(datas, batch_size=int(batch_size), shuffle=True, num_workers=4, pin_memory=True)

        if Xtest is not None:
            datas = _torch.utils.data.TensorDataset(_torch.tensor(Xtest))
            test_loader = _torch.utils.data.DataLoader(datas, batch_size=int(batch_size * 0.1), shuffle=False, num_workers=4)
        else:
            test_loader = None

        if Xval is not None:
            datas = _torch.utils.data.TensorDataset(_torch.tensor(Xval))
            validation_loader = _torch.utils.data.DataLoader(datas, batch_size=int(batch_size * 0.1), shuffle=False, num_workers=4)
        else:
            validation_loader = None

        return train_loader, validation_loader, test_loader

    def fix_mask(self, prior):

        if prior is not None:
            prior = prior.copy() if isinstance(prior, _np.ndarray) else prior.values.copy()
            if self._randomize_prior:
                prior = _randomize(prior)
            # mask = _torch.tensor(prior, dtype=self.astype, device=self.device)
            mask = _torch.tensor(prior, dtype=self.astype)

        else:
            mask = None

        # print('prior mask')
        # print(mask.shape)
        # print(mask[mask.nonzero(as_tuple=True)])
        return mask

    def setup_model(self):

        if self.cv_prior:
            prior, gs = split_prior(self.prior.copy(), gold_standard=self.gold_standard.copy(), rng=self.rng, fraction_gs=self.fraction_gs)
        else:
            prior = self.prior.copy()
            gs = self.gold_standard.copy() if self.gold_standard is not None else self.gold_standard

        modelkw = {**self.default_model_kwargs, **self.modelkw}
        mask = self.fix_mask(prior)

        model = self.model_class(mask, device=self.device, **modelkw).to(self.device)

        self.tmp_model = model
        return model, prior, gs

    def resetup_model(self, pre_model):

        modelkw = {**self.default_model_kwargs, **self.modelkw}

        for name, mask in pre_model.named_parameters():
            if 'prior' in name:
                in_mask = _deepcopy(mask.data.cpu().detach().bool().float())

            if 'grn' in name:
                out_mask = _deepcopy(mask.data.cpu().detach().bool().float())

        model = self.model_class(in_mask, mask_grn=out_mask, device=self.device, **modelkw).to(self.device)

        del pre_model

        # print('fine_tune model, before training')
        # print([i[1].abs().max().item() for i in model.named_parameters()])

        return model

    def train_epoch(self, model, train_loader):

        model.train()

        loss = 0
        for batch_features in train_loader:
            # print(batch_features[0])
            batch_features = batch_features[0]
            batch_features = batch_features.to(self.device)

            self.optimizer.zero_grad()

            # compute reconstructions
            outputs = model(batch_features)

            # compute training reconstruction loss
            train_loss = self.loss(outputs, batch_features)

            if (self.reg_loss is not None):
                if self.model_params_regularized is None:
                    train_loss = self.reg_loss.regularized_all_param(reg_loss_function=train_loss)
                else:
                    for model_param_name, model_param_value in model.named_parameters():
                        if model_param_name in self.model_params_regularized:
                            train_loss = self.reg_loss.regularized_param(param_weights=model_param_value, reg_loss_function=train_loss)

            # compute accumulated gradients
            train_loss.backward()

            # perform parameter update based on current gradients
            self.optimizer.step()

            # add the mini-batch training loss to epoch loss
            loss += float(train_loss.detach().item())

            del batch_features

        # compute the epoch training loss
        loss = loss / len(train_loader)

        return loss

    def validation_epoch(self, model, data_loader, reset_var=False):

        loss = 0

        # if reset_var:
        #     self.var = 0

        # if self.var == 0:
        #     var = 0
        # else:
        #     var = self.var
        var = 0
        model = model.eval()

        if hasattr(model.prior, 'mask'):
            notinP = (model.prior.mask.sum(0) == 0)
        with _torch.no_grad():

            for batch_features in data_loader:
                batch_features = batch_features[0]

                batch_features = batch_features.to(self.device)

                outputs = model(batch_features)

                if (not self.err_targets_in_prior and (notinP.sum() > 0)) and hasattr(model.prior, 'mask'):
                    val_loss = self.loss(outputs[:, notinP], batch_features[:, notinP])
                else:
                    val_loss = self.loss(outputs, batch_features)

                # if self.regularizer is not None and False:
                #     if self.model_params_regularized is None:
                #         val_loss = self.reg_loss.regularized_all_param(reg_loss_function=val_loss)
                #     else:
                #         for model_param_name, model_param_value in self.model.named_parameters():
                #             if model_param_name in self.model_params_regularized:
                #                 val_loss = self.reg_loss.regularized_param(param_weights=model_param_value, reg_loss_function=val_loss)

                loss += float(val_loss.detach().item())

                # if self.var == 0:
                val_var = self.loss(_torch.zeros(batch_features.shape).to(self.device), batch_features)
                var += float(val_var.detach().item())

                del batch_features

            R2 = 1 - (loss / var)  # len(data_loader)

            # if self.var == 0:
            #     self.var = var

            return loss / len(data_loader), R2

    def epoch_loop(self, model, train_loader, validation_loader=None, zeta=None):

        epochs = self.epochs if zeta > self.params['zeta'].min() else self.epochs * 3
        train_loss = []
        lr = self.params['lr']
        self.loss = self.loss_function(**self.default_loss_kwargs)
        self.optimizer = self.optimizer_class(model.parameters(), lr=lr, **self.optimizerkw)

        if (self.regularizer is not None) and (zeta is not None):
            self.reg_loss = self.regularizer(model=model)
            self.reg_loss.lambda_reg = zeta

            if self.regularizer.__name__ == 'ElasticNetRegularizer':
                self.reg_loss.alpha_reg = self.el_alpha

        else:
            self.reg_loss = None

        for epoch in range(epochs):

            self.global_epoch += 1
            tr_loss = self.train_epoch(model, train_loader)

            if (validation_loader is not None):
                val_loss, R2 = self.validation_epoch(model, validation_loader)
            else:
                val_loss = _np.nan
                R2 = 0

            train_loss.append([self.global_epoch, epoch, tr_loss, val_loss, R2])

            if self.verbose:

                zprint = zeta if zeta is not None else 0
                print(f"epoch: {epoch+1}/{epochs}, zeta: {zprint:.2e}, t-loss = {tr_loss:6.3g}, v-loss = {val_loss:6.3g}, v-R2 = {R2:4.3g}, BS = {self.bs}", end='\r')

        del model
        return train_loss

    def find_optimimal_params(self, _performance, groups=_NETGROUPS, use_randomized=False, metric='BIC'):

        df = _performance.groupby(groups).describe()
        idxmin = df[(metric, '50%')].groupby(level=groups[:-1]).idxmin()

        model_pass = df.loc[idxmin][(metric, '50%')] + df.loc[idxmin][(metric, 'std')]

        opt_params = []
        for (grps, df) in _performance.groupby(groups):

            data = df.describe()
            if (data.loc['50%', metric] < model_pass.loc[grps[:-1]]).tolist()[0]:
                opt_params.append(df)

        return _pd.concat(opt_params)

    def extract_optimal_models(self, _models, _performance, sparsest=True, groups=_NETGROUPS, x='zeta', logx=True, rounding_factor=1e3, merge_strat='median'):

        performance = _performance.copy()

        if logx:
            performance[x] = (_np.round(rounding_factor * _np.log10(performance[x])) / rounding_factor)

        tmp = (performance['ftzeta'] >= 0).replace([True, False], ['[ft]', ''])
        performance['layer'] = performance['layer'] + ' ' + tmp
        performance = performance.sort_values('layer')

        opt_models = {}
        if sparsest:
            obje = 'nnz'
        else:
            obje = 'BIC'

        optobj = {'all': _np.inf}
        curobj = {}
        for (grps, df) in performance.groupby(groups):

            layer = df['layer'].unique()[0].strip()
            optobj[layer] = optobj['all'] if layer not in optobj else optobj[layer]

            curobj[layer] = df.describe().loc['50%', obje]

            if curobj[layer] < optobj[layer]:
                optobj[layer] = curobj[layer]
                opt_models[layer] = self.extract_models(_models, df['hash'].tolist(), merge_strat=merge_strat)

        return opt_models

    def extract_models(self, _models, hashes, merge_strat='median'):

        from collections import OrderedDict
        with _torch.no_grad():
            if isinstance(hashes, str):
                hashes = [hashes]

            output_net = _models[hashes[0]]

            if len(hashes) == 1:
                return output_net

            onet = OrderedDict()
            for k, v in output_net.items():
                __ = []
                for i in range(len(hashes)):
                    tmp = _models[hashes[i]]
                    __.append(tmp[k])

                    # output_net[k][:, :, i] = v

                onet[k] = _torch.stack(__, dim=2)
                tmp = getattr(_torch, merge_strat)(onet[k], 2)

                onet[k] = tmp[0] if len(tmp) == 2 else tmp

        return onet

    def plot_performance(self, _performance, metrics=['AUPR', 'R2'], x='zeta', logx=True, style_label='default', figsize=(10, 4), facecolor='w', edgecolor='k', constrained_layout=True, dpi=None, plot_randomized=True, bbox_to_anchor=(1.01, 0.6), plotdots=False, rounding_factor=1e3):

        import seaborn as _sns
        import matplotlib.pyplot as _plt

        performance = _performance.copy()

        tmp = (performance['ftzeta'] >= 0).replace([True, False], ['[ft]', ''])
        performance['layer'] = performance['layer'] + ' ' + tmp
        performance = performance.sort_values('layer')

        layers = performance['layer'].unique().tolist()
        nlayers = len(layers)

        if logx:
            performance[f'log({x})'] = (_np.round(rounding_factor * _np.log10(performance[x])) / rounding_factor)
            x = f'log({x})'
            xticks = performance[x].sort_values().unique()
        else:
            xticks = performance[x].sort_values().unique()

        figs = []
        axs = []
        with _plt.style.context(style_label):

            for metric in metrics:
                fig = _plt.figure(figsize=figsize, facecolor=facecolor, edgecolor=edgecolor, constrained_layout=constrained_layout, dpi=dpi)
                ax = fig.add_subplot(1, 1, 1)
                # _sns.despine()

                # for rand, df in performance.groupby(['random P', 'random X', 'ftzeta']):
                #     ftz = rand[-1]
                #     rand = rand[:2]
                for rand, df in performance.groupby(['random P', 'random X']):
                    rand = rand[:2]
                    df = df.copy()
                    zorder = int(''.join([str(int(i)) for i in rand]), 2)
                    # palette = ['deep', 'Greys', 'Blues', 'Reds']
                    palette = ['Paired', 'Greys', 'Blues', 'Reds']
                    if any(rand):
                        if not plot_randomized:
                            continue

                        df['layer'] = df['layer'] + ', randomized P' if rand[0] else df['layer'] + ', randomized X'

                    pal = palette[zorder]
                    # pal = 'pastel' if ((ftz != 1) and (pal == 'deep')) else pal
                    g = _sns.pointplot(x=x, y=metric,
                                       data=df,
                                       dodge=0.5, join=False,
                                       palette=pal, hue='layer',
                                       markers="d", scale=1.0, ci=95,
                                       estimator=_np.median, ax=ax, zorder=-zorder)

                    handles, labels = ax.get_legend_handles_labels()

                    l = ax.legend(
                        # handles[nlayers:nlayers * 3], labels[nlayers:nlayers * 3],
                        title="Performance",
                        handletextpad=0, columnspacing=1,
                        loc="upper left", frameon=True, bbox_to_anchor=bbox_to_anchor)
                    l.get_title().set_fontsize(16)

                    ax.set_xticklabels([f'{s:.2f}' for s in xticks])
                    ax.grid(axis='y', linewidth=0.5)
                    # xlbl = ax.xaxis.get_label()
                    # ax.set_xlabel(xlbl.get_text() if (not logx) else f'log({xlbl.get_text()})')

                # plt.plot(training.performance['zeta'].values, [v['grn.linear.weight'].max().item() for k, v in training._models.items()])

                figs.append(fig)
                axs.append(ax)

        return figs, axs

    def compile_run_params(self, model, losses, zeta):

        if hasattr(zeta, '__iter__'):
            zeta, ftzeta = zeta[0], zeta[1]
        else:
            ftzeta = -1

        results = _pd.DataFrame(losses, columns=['Global', 'epoch', 'train err', 'val err', 'R2'])

        for k, v in self.optimizerkw.items():
            results[k] = v

        results['lr'] = self.params['lr']
        results['zeta'] = zeta
        results['ftzeta'] = ftzeta
        results['BS'] = self.bs
        results['epochs'] = self.epochs

        # if hasattr(self, '_results'):
        #     self._results = _pd.concat([self._results, results], ignore_index=True)
        # else:
        #     self._results = results

        return results

    def store_model(self, model):

        state = self.state
        # self._models[state] = _deepcopy(model.cpu().state_dict())
        modstate = _deepcopy(model.state_dict())

        for k in modstate:
            modstate[k] = modstate[k].cpu().detach()

        self._models[state] = _deepcopy(modstate)

        del modstate

    def evaluate(self, model, prior, gold_standard, zeta=None, results=None, data_loader=None):

        import hashlib
        performance = []

        if hasattr(zeta, '__iter__'):
            zeta, ftzeta = zeta[0], zeta[1]
        else:
            ftzeta = -1

        outpwall, inpw = extract_weights(model, prior)
        pr = _RSPR([outpwall.T.abs()], gold_standard.T.abs())
        mcc = _RSMCC([outpwall.T.abs()], gold_standard.T.abs())

        optconf, fracgs, recall, prec = compute_performance_opt_mcc(pr.confidence_data, mcc.confidence_data['MCC'])

        active_tfs = (outpwall.astype(bool).sum(1) > 0).sum()
        active_ptfs = (inpw.astype(bool).sum(1) > 0).sum()
        nnzout = (outpwall != 0).sum().sum()
        outs = _np.prod(outpwall.shape)
        nnzin = (inpw != 0).sum().sum()

        performance.append([mcc.maxmcc, mcc.nnzmcc, pr.aupr, prec, recall, optconf, fracgs, float(nnzout), nnzout / float(outs), float(active_tfs), float(nnzin), float(nnzin) / float(prior.astype(bool).sum().sum()), float(active_ptfs), self.bs, self.epochs])

        performance = _pd.DataFrame(performance, columns=['OPT_MCC', 'MCC', 'AUPR', 'OPT_PREC', 'OPT_REC', 'OPT_CONF', 'FRAC_GS', 'nnz', 'nnz frac', 'nnz tf', 'nnzp', 'nnzp frac', 'nnzp tf', 'BS', 'epochs'])

        if data_loader is not None:
            bic = _bic(data_loader, model, device=self.device, lossf=self.loss_function(), eps=self.eps)

        if results is not None:
            errvals = ['train err', 'val err', 'R2']
            for val, err in zip(errvals, results[errvals].iloc[-1, :]):
                performance[val] = err

        performance['BIC'] = _torch.median(bic).item()
        performance['loss'] = self.loss_function.__name__
        performance['regulation'] = self.regularizer.__name__
        performance['optimizer'] = self.optimizer_class.__name__
        performance['random P'] = self._randomize_prior
        performance['random X'] = self._randomize_data
        performance['zeta'] = zeta
        performance['ftzeta'] = ftzeta

        for k, v in self.optimizerkw.items():
            performance[k] = v

        performance['layer'] = self.use_layer

        state_values = performance.tail(1).values.flatten().tolist()
        hash = hashlib.sha1(str(state_values).encode('utf-8')).hexdigest()

        performance['hash'] = hash
        if results is not None:
            results['hash'] = hash
        self.state = hash

        return performance, results

    def _structure_run_output(self, model, losses, zeta, use_prior, use_gs=None, data_loader=None):

        results = self.compile_run_params(model, losses, zeta)
        performance, results = self.evaluate(model, use_prior, use_gs if use_gs is not None else use_prior, zeta=zeta, results=results, data_loader=data_loader)

        if hasattr(self, '_performance'):
            self._performance = _pd.concat([self._performance, performance], 0, ignore_index=True)
        else:
            self._performance = _pd.DataFrame.from_dict(performance)

        if hasattr(self, '_results'):
            self._results = _pd.concat([self._results, results], ignore_index=True)
        else:
            self._results = results

        return performance

    def _truncate_weights(self, model):

        with _torch.no_grad():
            state = model.state_dict()
            for p_name in state:
                # if 'grn' in p_name:
                # state[p_name] = _F.hardshrink(state[p_name], lambd=self.eps)
                state[p_name][state[p_name].abs() < self.eps] = 0

    def resample(self, Xtrain, Xval, Xtest, y=None):

        train_l, val_l, test_l = self.setup_data(Xtrain, Xval=Xval, Xtest=Xtest, **self.datakw)
        model, use_prior, use_gs = self.setup_model()

        for zeta in self.params['zeta'] if hasattr(self.params['zeta'], '__iter__') else [self.params['zeta']]:

            losses = self.epoch_loop(model, train_l, validation_loader=val_l, zeta=zeta)
            self._truncate_weights(model)
            performance = self._structure_run_output(model, losses, zeta, use_prior, use_gs=use_gs, data_loader=train_l)
            self.store_model(model)

            if self.verbose:
                del performance['BS'], performance['epochs'], performance['OPT_MCC']
                print(_tbl(performance, headers='keys' if self.hdr else (), tablefmt='simple' if self.hdr else 'plain', showindex=False, floatfmt='10.3g'))
                self.hdr = False

            if self._fine_tune:  # Can't get this to work.
                ftmodel = self.resetup_model(model)
                losses = self.epoch_loop(ftmodel, train_l, validation_loader=val_l, zeta=self.ftzeta)
                # self._truncate_weights(ftmodel)
                # print('fine_tune model, after training')
                # print([i[1].abs().max().item() for i in ftmodel.named_parameters()])
                performance = self._structure_run_output(ftmodel, losses, [zeta, self.ftzeta], use_prior, use_gs=use_gs, data_loader=train_l)

                self.store_model(ftmodel)

                del ftmodel

                if self.verbose:
                    del performance['BS'], performance['epochs'], performance['OPT_MCC']
                    print(_tbl(performance, headers='keys' if self.hdr else (), tablefmt='simple' if self.hdr else 'plain', showindex=False, floatfmt='10.3g'))
                    self.hdr = False

            if (performance['nnz'].values[0] == 0) and self._zero_break:
                if self.verbose:
                    print('zero network generated. Breaking zeta loop.')

                del model
                return None

        del model

    def loop_params(self, X, y=None):

        Xtrain, Xtest, Xval = self.split_samples(X, y=y, **self.datakw)
        for bs in _np.arange(self.n_resamples + 1):
            self.bs = bs
            self.resample(Xtrain, Xval, Xtest, y=y)

    def run(self, data, y=None, epochs=100, use_layer=None, batch_size=2048, datakw={}, modelkw={}, optimizerkw={}, lr=1e-3, zetavec=None, reset_model=True, el_alpha=0.0, randomize_data=False, randomize_prior=False, fine_tune=False, ftzeta=None):

        self.hdr = True
        self.epochs = epochs
        self.params = {'zeta': zetavec, 'lr': lr}
        self.optimizerkw = optimizerkw
        self.datakw = {**self.default_data_kwargs, **datakw}
        self.modelkw = modelkw
        self.el_alpha = el_alpha
        self.use_layer = use_layer if (use_layer is not None) else 'X'
        self._randomize_data = randomize_data
        self._randomize_prior = randomize_prior
        self._fine_tune = fine_tune
        self.ftzeta = self.eps if ftzeta is None else ftzeta

        X, y = _get_layer_data(data, y=y, use_layer=use_layer, shuffle_samples=None, copy=True)

        self.loop_params(X, y=y)

        # _torch.save(self.best_model, '{}_best_model.pt'.format(self.result_file))
