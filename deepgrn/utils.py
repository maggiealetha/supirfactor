import torch as _torch
from anndata import AnnData as _AnnData


def get_device():
    if _torch.cuda.is_available():
        device = 'cuda'
    else:
        device = 'cpu'
    return _torch.device(device)


def _get_layer_data(data, y=None, use_layer=None, shuffle_samples=None, copy=True):

    if isinstance(data, _AnnData):
        # adata = data.copy() if copy else data
        if (use_layer == 'X') or (use_layer is None):
            X = data.X.copy()
        elif use_layer == 'raw':
            X = data.raw.X.copy()
        else:
            X = data.layers[use_layer].copy()

    else:
        X = data.copy()

    if shuffle_samples is not None:
        s = _torch.arange(X.shape[0])
        shuffle_samples.shuffle(s)

        X = X[s, :]

        if y is not None:
            y = y[s]

    return X, y
