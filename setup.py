from setuptools import setup

DISTNAME = 'SupirFactor'
VERSION = '0.1'
DESCRIPTION = "Using deep learning methods to derive gene regulatory network strucutre."
# with open('README.rst') as f:
#     LONG_DESCRIPTION = f.read()
MAINTAINER = 'Andreas Tjarnberg'
MAINTAINER_EMAIL = 'andreas.tjarnberg@nyu.edu'
URL = 'https://gitlab.com/Xparx/deepGRN'
DOWNLOAD_URL = ''
LICENSE = 'LGPL'


setup(name=DISTNAME,
      version=VERSION,
      description=DESCRIPTION,
      url=URL,
      author=MAINTAINER,
      author_email=MAINTAINER_EMAIL,
      license=LICENSE,
      packages=['deepgrn'],
      python_requires='>=3.7',
      install_requires=[
          'torch',
          'scipy',
          'sklearn',
          'pandas',
          'scanpy',
          'matplotlib',
          'seaborn',
          'tabulate',
      ],
      zip_safe=False)
